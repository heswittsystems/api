<html>
<head>
<script src="https://js.braintreegateway.com/web/dropin/1.26.1/js/dropin.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/css">
.button {
  cursor: pointer;
  font-weight: 500;
  left: 3px;
  line-height: inherit;
  position: relative;
  text-decoration: none;
  text-align: center;
  border-style: solid;
  border-width: 1px;
  border-radius: 3px;
  -webkit-appearance: none;
  -moz-appearance: none;
  display: inline-block;
}

.button--small {
  padding: 10px 20px;
  font-size: 0.875rem;
}

.button--green {
  outline: none;
  background-color: #64d18a;
  border-color: #64d18a;
  color: white;
  transition: all 200ms ease;
}

.button--green:hover {
  background-color: #8bdda8;
  color: white;
}


</script>

</head>
<body>

<div id="dropin-container"></div>
<button id="submit-button" class="button button--small button--green">Purchase</button>
<?php
	ini_set('display_errors',1);
	require_once('./lib/Braintree.php');
	$gateway = new Braintree\Gateway([
	    'environment' => 'sandbox',
	    'merchantId' => '7rzbkwcwh8syh5gk',
	    'publicKey' => '9sbj3sjpnyhrypwh',
	    'privateKey' => '7de1f64d27ea2575756ab8f79a9309f6'
	]);

    
     $clientToken = $gateway->clientToken()->generate();
     $amt = $_GET['amount'];
?>

<script type="text/javascript">

var button = document.querySelector('#submit-button');
var token = '<?php echo $clientToken; ?>';
var amount = '<?php echo $amt; ?>';
braintree.dropin.create({
  authorization: token,
  selector: '#dropin-container'
}, function (err, instance) {
  button.addEventListener('click', function () {
    instance.requestPaymentMethod(function (err, payload) {
      // Submit payload.nonce to your server
      console.log("nonce found" + payload.nonce);
      $.ajax({
            url: "handle.php",
	    type: "POST",
	    data: {nonce : payload.nonce,amount: amount},
            cache: false,
            success: function(html){
                  console.log("done");
              }
       });
      
    });
  })
});


</script>


</body>
</html>
