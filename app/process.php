<?php
/*

data structure
params: {

method: function to call eg login
data: { parameters for each function }

}

return structure

resp: {
  code:
  desc:
  response: {json array of data}
 
}
**/
require_once('login.php');
$input = file_get_contents("php://input");
error_log($input);
$data = json_decode($input,true);
//print_r($data['data']);
$reply = call_user_func($data['method'], $data['data']);
echo json_encode($reply);



?>
