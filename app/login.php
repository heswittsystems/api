<?php
//login("admin","admin1234");
require_once('functions.php');
function login($data)
{
//         var_dump($data);
        $username = $data['username'];
	$password = $data['password'];
	$ch = curl_init('https://portal.nckochama.org/index.php?p=/entry/signin&Target=discussions');

	//$cookiesFile = "/var/www/html/airtime/ncko/api/app/cookies.txt"; // <--- cookies are stored here
	$cookiesFile = tempnam ("/tmp/", "CURLCOOKIE");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, false); // <---

	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiesFile ); // <---
	curl_setopt($ch, CURLOPT_COOKIEJAR,  $cookiesFile ); // <---

	$result = curl_exec($ch);

	curl_close($ch);

	if( $result === false ) {
	    // failure
	}
        $doc = new DOMDocument();
	$doc->loadHTML($result);
	$element = $doc->getElementById('Form_TransientKey');
	$key = $element->getAttribute('value');
	$element = $doc->getElementById('Form_ClientHour');
	$ch = $element->getAttribute('value');

	//$cookiesFile = "/var/www/html/airtime/ncko/api/app/cookies.txt"; // <--- cookies are retrieved here

	$curl = curl_init( 'https://portal.nckochama.org/index.php?p=/entry/signin' );
	curl_setopt( $curl, CURLOPT_HTTPHEADER, array( 'Content-Type: application/x-www-form-urlencoded' ) );
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt( $curl, CURLOPT_COOKIEFILE, $cookiesFile ); // <---
	curl_setopt( $curl, CURLOPT_COOKIEJAR,  $cookiesFile ); // <---
	curl_setopt($curl, CURLOPT_POST, 1 ); // <---
	curl_setopt($curl, CURLOPT_POSTFIELDS,  'TransientKey='.$key.'&ClientHour='.urlencode($ch).'&Email='.urlencode($username).'&Password='.urlencode($password).'&Sign_In=Sign%20In&Checkboxes%5B%5D=RememberMe&RememberMe=1' ); // <---
	curl_setopt($curl, CURLOPT_VERBOSE, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	$verbose = fopen('php://temp', 'w+');
	curl_setopt($curl, CURLOPT_STDERR, $verbose);


	$resp = curl_exec( $curl );
	//var_dump($resp);
	$pos = strpos($resp,"New Discussion");
	//$pos;

	if ($resp === FALSE) {
	    printf("cUrl error (#%d): %s<br>\n", curl_errno($curl),
		       htmlspecialchars(curl_error($curl)));
		       }
	/*
		       rewind($verbose);
		       $verboseLog = stream_get_contents($verbose);

		       echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
	*/
	curl_close($curl);
	$response = array();
	if(!$pos > 0)
	{
	  $response['code'] = 300;
	  $response['desc'] = "Invalid Login Credentials";
	  return $response;
	}

         $det =  app_get_user_details($username);
	  $response['code'] = 100;
	  $response['desc'] = "Login OK";
	  $response['result'] = array('names'=>$det->name,'member_id'=>$det->userid);
	  return $response;
	  
}

function balance($data)
{
          $user_id = $data['member_id'];
   	  $bal = get_balance($user_id);  
	  $response['code'] = 100;
	  $response['desc'] = "OK";
	  $response['result'] = array('balance'=>number_format($bal,2));
	  return $response;

}
function statement($data)
{
    $user_id = $data['member_id'];
    $acc = Memberaccmappings::find($user_id)->acc_no;
    error_log($acc);
    $recs = Accstatements::find_by_sql("select * from accstatements where acc_no='$acc' order by id desc");
    //var_dump($recs);
    $resp = array();
    foreach($recs as $rec)
    {
       $res = array();
       if($rec->debit > 0)
       {
          $res['amount'] = $rec->debit; 
          $res['type'] = 'subtract';
       }
       else
       {
	 $res['amount'] = $rec->credit;
	 $res['type'] = 'add';

       }
       $res['balance'] = $rec->balance;
       $res['desc'] = $rec->tx_desc;
       $res['ref'] = $rec->ref;
       $res['tx_date'] =  date_string($rec->tx_date);
       $res['id'] =  $rec->id;
       $resp[] = $res;

    }
	  $response['code'] = 100;
	  $response['desc'] = "OK";
	  $response['result'] = $resp;
	  return $response;
}

function regtoken($data)
{
          $user_id = $data['member_id'];
	  $token = $data['token'];
	  try
	  {
	   $t = Pushtokens::find($user_id);
	   $t->delete();
	  }
	  catch(Exception $e)
	  {

	  }
	  $offline = new Pushtokens(array('user_id'=>$user_id,'push_token'=>$token));
	  $offline->save();
	  $response['code'] = 100;
	  $response['desc'] = "OK";
	  $response['result'] = array('resp'=>'Token Saved');
	  return $response;

}

function offline($data)
{
          $user_id = $data['member_id'];
	  $amount = $data['amount'];
	  $ref  = $data['ref'];
	  $date  = $data['tx_date'];
	  $method  = $data['method'];//integer
	  $offline = new Offlinepayments(array('member_id'=>$user_id,'method_id'=>$method,'amount'=>$amount,'tx_ref'=>$ref,'date_paid'=>$date,'tx_status'=>1));
	  $offline->save();
	  $response['code'] = 100;
	  $response['desc'] = "OK";
	  $response['result'] = array('resp'=>'Offline deposit received. Awaiting confirmation');
	  return $response;

}

function cases($data)
{
    $user_id = $data['member_id'];
    $cases = Welfareprojects::find('all',array('conditions'=>array('status = ?','Open')));
    $res = array();
    foreach($cases as $case)
    {
       $resp = array();
       $resp['id'] = $case->id;
       $resp['amount'] = $case->amount;
       $resp['name'] = $case->project_name;
       $resp['tdate'] =  date_string($case->date_added);
       $resp['paid'] = check_recoup($user_id,$case->id);
       $res[] = $resp;

    }
    
    $response['code'] = 100;
    $response['desc'] = "OK";
    $response['result'] = $res;
    return $response;
  

}

function check_recoup($member_id,$case_id)
{
  $check = Caserecoups::find_by_sql("select * from caserecoups where member_id ='$member_id' and case_id='$case_id'");
  if(count($check) > 0)
  {
    return 1;

  }
  return 2;

}

function newchat($data)
{
    $user_id = $data['member_id'];
    $subject = $data['subject'];
    $msg = $data['message'];
    new_chat($user_id,$subject,$msg,$user_id);
    $response['code'] = 100;
    $response['desc'] = "OK";

    return $response;

}
function updatechat($data)
{
    $user_id = $data['member_id'];
    $subject_id = $data['subject_id'];
    $msg = $data['message'];

    update_chat($subject_id,$user_id,$msg);
    $response['code'] = 100;
    $response['desc'] = "OK";

    return $response;

}

function events($data)
{
   $user_id = $data['member_id'];
   if(count($data) > 1)
   {
      $event_id = $data['eid'];
      $action = $data['action'];
      $ea = new Eventattendees(array('event_id'=>$event_id,'member_id'=>$user_id,'status'=>$action));
      $ea->save();

   }
   $events = Events::find('all',array('order' => 'event_date desc'));
   $all = array();
   foreach($events as $event)
   {
     $data = array();
     $data['id'] = $event->id;
     $data['title'] = $event->event_title;
     $data['desc'] = $event->event_details;
     $data['edate'] = date('l jS  F Y h:i A',$event->event_date);
     if(time() > $event->event_date)//event already past
     {
        $active = 1;
     }
     else
     {
       $active = 2;

     }
     $status = Eventattendees::find_by_sql(" SELECT status FROM  eventattendees where member_id='$user_id' and event_id = '".$event->id."' UNION SELECT 0 LIMIT 1");
     $data['status'] = $status[0]->status;
     $data['active'] = $active;


     $all[] = $data;
   }
   $response['events'] = $all;
   $response['code'] = 100;
   $response['desc'] = "OK";
   return $response;


}

function notifications($data)
{
   $user_id = $data['member_id'];
   $nots = Notifications::find_by_sql("select * from notifications where member_id='$user_id' order by id desc");
   $all = array();
   foreach($nots as $n)
   {
     $data = array();
     $data['id'] = $n->id;
     $data['title'] = $n->title;
     $data['desc'] = $n->details;
     $data['date_added'] = date('l jS  F Y h:i A',strtotime(ActiveRecord\Connection::datetime_to_string($n->date_added)));
     $data['status'] = $n->status;

     $all[] = $data;
     update_notification($n->id);
     
   }
   $response['notifications'] = $all;
   $response['code'] = 100;
   $response['desc'] = "OK";
   return $response;

}
function update_notification($n_id)
{
   $n =  Notifications::find($n_id);
   if($n->status == 3)
   {
     return;

   }
   $n->status = $n->status + 1;
   $n->save();


}
function allchats($data)
{
   $user_id = $data['member_id'];
   $nots = Messagesubjects::find_by_sql("select * from messagesubjects where recipient_id='$user_id' order by id desc");
   $all = array();
   foreach($nots as $n)
   {
     $data = array();
     $data['id'] = $n->id;
     $data['title'] = $n->subject;
     $data['date_added'] = date('l jS  F Y h:i A',strtotime(ActiveRecord\Connection::datetime_to_string($n->created_date)));
     $data['status'] = 1;// there are unread chats

     $all[] = $data;
     
   }
   $response['chats'] = $all;
   $response['code'] = 100;
   $response['desc'] = "OK";
   return $response;

}

function singlechat($data)
{
        $user_id = $data['member_id'];
        $chat_id = $data['chat_id'];
        if(strlen($data['message']) > 2)
	{
	   update_chat($chat_id,$user_id,$data['message']);
	}
	error_log("select * from messages where subject_id='".$chat_id."' order by seq_no asc");
	$chats = Messages::find_by_sql("select * from messages where subject_id='".$chat_id."' order by seq_no asc");
	$subject = Messagesubjects::find($chat_id);
	$all_html = '';
	$incoming_html = 
	$c = count($chats);
	$i = 0;
	$css = array();
	$all = array();
	while($i < $c)
	{
	   if($i > 0)//check if the author of current message is same as previous message
	   {
	     if($chats[$i]->created_by == $chats[$i - 1]->created_by)//two consecutive messages by same person
	     {
		  $css[$i] = $css[$i - 1]; //use previous style

	     }
	     else
	     {
		if($css[$i - 1]  == "outgoing")
		{
		   $css[$i] = "incoming";
		}
		if($css[$i - 1]  == "incoming")
		{
		   $css[$i] = "outgoing";
		}

	     }
	   }   
	   else
	   {
	     $css[$i] = "outgoing";
	   }
	   $chat = array();
	   //var_dump($chats[$i]);
	   $chat['msg'] = $chats[$i]->message; 
	   $chat['created_date'] = date('m-d-Y h:i A', strtotime(ActiveRecord\Connection::datetime_to_string($chats[$i]->created_date)));
	   $chat['status'] = $css[$i]; 
	   $chat['id'] = $chats[$i]->id; 
	   $all[] = $chat;
	   $i++;
	}
	   
	$response['chats']['msgs'] = $all;
	$response['chats']['title'] = $subject->subject;
	$response['code'] = 100;
	$response['desc'] = "OK";
	return $response;

}



?>
