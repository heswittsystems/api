<style type="text/css">

.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
  display: none;
   margin: 0 auto;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}




</style>
<div class="grid-form">
 		<div class="grid-form1">
 		<h3 id="forms-example" class="">Create New Case</h3>
 		<form onsubmit="return confirm('Do you really want to add the Case? This will trigger automatic recoups.')" action="" method="post"  name="RequestCreateForm" id="RequestCreateForm">
  <div class="form-group">
    <label for="exampleInputEmail1">Name of Case</label>
    <input type="text" class="form-control" id="exampleInputEmail1" required name="case" placeholder="Enter name of the case">
  </div>
  <div class="form-group">
    <label for="amount">Amount for the Case</label>
    <input type="number" class="form-control" required id="amount" name="amount" placeholder="Enter the amount for the case">
  </div>
  <div class="form-group">
    <input type="submit" onclick="document.getElementById('loader').style.display = 'block';"  name="add" value="Add Case" class="btn btn-primary" style="background-color: #4CAF50;" >
  </div>
</form>
<div align="center" id="loader" class="loader"></div>
<?php
if($_POST['add'])
{
     ?>
<script type="text/javascript">document.getElementById('loader').style.display = 'block';</script>
<?php
     //print_r($_POST);
     //ob_flush();
     try
     {
     $wp = new Welfareprojects(array('project_name'=>$_POST['case'],'amount'=>$_POST['amount'],'status'=>'Open'));
     $wp->save();
     
     //var_dump($wp);
     $case_id = $wp->id;
      gen_recoup_acc($case_id);
     $members = Memberaccmappings::find('all');
     $i = 0;
     $title = "New Case: ".$_POST['case'];
     $body = "New case for ".$_POST['case'].". Recoup amount is USD ".$_POST['amount'];
     $to = array();
     foreach($members as $member)
     {
         $notice = new Notifications(array('title'=>$title,'details'=>$body,'member_id'=>$member->member_id));

	 $notice->save();
         try
	 {
	   $token =  Pushtokens::find($member->member_id)->push_token;
	   $to[] = '"'.$token.'"';
	//var_dump($member);
	 }
	catch(Exception $e)
	{


	}
	   case_recoup($member->member_id,$case_id);


     }

     $all = array_chunk($to,90);
     foreach($all as $a)
     {

        $tokens = implode(",",$a);
	send_push_notification($tokens,$title,$body);

     }
     ?>
<script type="text/javascript">document.getElementById('loader').style.display = 'none';</script>
    <div class="alert alert-success" role="alert">
      New case added and automatic recoup done. Please check recoup report for details
      </div>
<?php
    }
    catch(Exception $e)
    {
      var_dump($e);
         ?>
	 <div class="alert alert-danger" role="alert">
	   An error has occurred. 
	   </div>
	   <?php

    }
}
?>
</div>
</div>
