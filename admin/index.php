<?php

require_once('functions.php');
/*$acc = new Accounts(array('acc_type' => 1));
var_dump($acc->save());
*/
?>
<html>
<head>
<title>NCKO Chama Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<script src="js/jquery.min.js"> </script>
<script src="js/bootstrap.min.js"> </script>
  
<!-- Mainly scripts -->
<script src="js/jquery.metisMenu.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<link href="css/custom.css" rel="stylesheet">
<script src="js/custom.js"></script>
<script src="js/screenfull.js"></script>
		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});
			

			
		});
		</script>



</head>
<body>
<div id="wrapper">
       <!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="index.html">NCKO Chama</a></h1>         
			   </div>
			 <div class=" border-bottom">
        	<div class="full-left">
        	  
            <div class="clearfix"> </div>
           </div>
     
       
            <!-- Brand and toggle get grouped for better mobile display -->
		 
		   <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="drop-men" >
		     
		     </div><!-- /.navbar-collapse -->
			<div class="clearfix">
       
     </div>
	  
		    <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
				
             <!--       <li>
                        <a href="index.php" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>
                    </li>
                 -->  
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Payments Made</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="index.php?id=1" class=" hvr-bounce-to-right"> <i class="fa fa-cloud-upload nav_icon"></i>Online Payments</a></li>
                            
                            <li><a href="index.php?id=2" class=" hvr-bounce-to-right"><i class="fa fa-money nav_icon"></i>Offline Payments</a></li>
			
					   </ul>
                    </li>
					<li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Account Statements</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="index.php?id=3" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Recoup Statement</a></li>
                            
                            <li><a href="index.php?id=4" class=" hvr-bounce-to-right"><i class="fa fa-user nav_icon"></i>Member Statement</a></li>


					   </ul>
					   </li>
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Cases</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="index.php?id=5" class=" hvr-bounce-to-right"> <i class="fa fa-gratipay nav_icon"></i>New Case</a></li>
                            
                            <li><a href="index.php?id=6" class=" hvr-bounce-to-right"><i class="fa fa-bar-chart-o nav_icon"></i>Case Report</a></li>
			
					   </ul>
                    </li>
					<li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Notifications</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="index.php?id=7" class=" hvr-bounce-to-right"> <i class="fa fa-bullhorn nav_icon"></i>Announcements</a></li>
                            
                            <li><a href="index.php?id=8" class=" hvr-bounce-to-right"><i class="fa fa-wechat nav_icon"></i>Member Messages</a></li>
                            <li><a href="index.php?id=9" class=" hvr-bounce-to-right"><i class="fa fa-calendar nav_icon"></i>Events Calendar</a></li>


					   </ul>
					   </li>
                    <!--</li>
					 <li>
                        <a href="inbox.html" class=" hvr-bounce-to-right"><i class="fa fa-inbox nav_icon"></i> <span class="nav-label">Inbox</span> </a>
                    </li>
                    
                    <li>
                        <a href="gallery.html" class=" hvr-bounce-to-right"><i class="fa fa-picture-o nav_icon"></i> <span class="nav-label">Gallery</span> </a>
                    </li>
                     <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-desktop nav_icon"></i> <span class="nav-label">Pages</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="404.html" class=" hvr-bounce-to-right"> <i class="fa fa-info-circle nav_icon"></i>Error 404</a></li>
                            <li><a href="faq.html" class=" hvr-bounce-to-right"><i class="fa fa-question-circle nav_icon"></i>FAQ</a></li>
                            <li><a href="blank.html" class=" hvr-bounce-to-right"><i class="fa fa-file-o nav_icon"></i>Blank</a></li>
                       </ul>
                    </li>
                     <li>
                        <a href="layout.html" class=" hvr-bounce-to-right"><i class="fa fa-th nav_icon"></i> <span class="nav-label">Grid Layouts</span> </a>
                    </li>
                   
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">Forms</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="forms.html" class=" hvr-bounce-to-right"><i class="fa fa-align-left nav_icon"></i>Basic forms</a></li>
                            <li><a href="validation.html" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>Validation</a></li>
                        </ul>
                    </li>
                   
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="signin.html" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Signin</a></li>
                            <li><a href="signup.html" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Singup</a></li>
                        </ul>
                    </li>-->
                </ul>
            </div>
			</div>
        </nav>
		 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	
 	 <!--faq-->
 	<div class="blank">
	        <?php
			if($_GET['id'] == "1")
			{
				include('online_payments.php');
			}
			if($_GET['id'] == "2")
			{
				include('offline_payments.php');
			}
			if($_GET['id'] == "3")
			{
				include('view_recoups.php');
			}
			if($_GET['id'] == "4")
			{
				include('view_members.php');
			}
			if($_GET['id'] == "5")
			{
				include('new_cases.php');
			}
			if($_GET['id'] == "6")
			{
				include('all_cases.php');
			}
			if($_GET['id'] == "7")
			{
				include('new_announcement.php');
			}
			if($_GET['id'] == "8")
			{
				include('all_chats.php');
			}
			if($_GET['id'] == "9")
			{
				include('events.php');
			}
			if($_GET['id'] == "11")
			{
				include('member_statement.php');
			}
			if($_GET['id'] == "12")
			{
				include('recoup_statement.php');
			}
			if($_GET['id'] == "13")
			{
				include('chats.php');
			}
			
			
			
			?>

			
	       </div>
	
	<!--//faq-->
		<!---->
<div class="copy">
            <p> &copy; <?php echo date('Y'); ?> NCKO Chama </p>	    </div>
		</div>
		</div>
		<div class="clearfix"> </div>
       </div>
     
<!---->
	
</body>
</html>

