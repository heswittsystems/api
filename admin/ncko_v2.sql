-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2021 at 06:30 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ncko`
--
CREATE DATABASE IF NOT EXISTS `ncko` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ncko`;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `acc_type` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `acc_type`, `date_added`) VALUES
(1, 1, '2021-03-05 15:34:21'),
(2, 1, '2021-03-05 15:49:00'),
(3, 1, '2021-03-05 15:50:57'),
(4, 1, '2021-03-05 15:51:08'),
(5, 1, '2021-03-05 15:54:54'),
(6, 1, '2021-03-05 16:00:23'),
(7, 1, '2021-03-05 16:00:40'),
(8, 1, '2021-03-05 16:08:14'),
(9, 1, '2021-03-05 16:09:02'),
(10, 1, '2021-03-05 16:09:59'),
(11, 1, '2021-03-05 16:10:41'),
(12, 1, '2021-03-05 16:11:25'),
(13, 1, '2021-03-05 16:11:56'),
(14, 1, '2021-03-05 16:13:43'),
(15, 1, '2021-03-05 16:16:18'),
(16, 1, '2021-03-05 16:17:35'),
(17, 1, '2021-03-05 16:17:49'),
(18, 1, '2021-03-05 16:18:29'),
(19, 1, '2021-03-05 16:18:35'),
(20, 1, '2021-03-05 16:18:44'),
(21, 1, '2021-03-05 16:19:22'),
(22, 1, '2021-03-05 16:21:51'),
(23, 1, '2021-03-05 16:22:24'),
(24, 1, '2021-03-05 16:26:18'),
(25, 1, '2021-03-05 16:27:48'),
(26, 1, '2021-03-05 16:28:43'),
(27, 1, '2021-03-05 16:30:00'),
(28, 1, '2021-03-05 16:30:16'),
(29, 1, '2021-03-05 16:31:47');

-- --------------------------------------------------------

--
-- Table structure for table `accstatement`
--

DROP TABLE IF EXISTS `accstatement`;
CREATE TABLE `accstatement` (
  `id` int(11) NOT NULL,
  `acc_no` int(11) NOT NULL,
  `ref` varchar(20) NOT NULL,
  `debit` int(11) NOT NULL,
  `credit` int(111) NOT NULL,
  `balance` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `acctypes`
--

DROP TABLE IF EXISTS `acctypes`;
CREATE TABLE `acctypes` (
  `id` int(11) NOT NULL,
  `acc_desc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acctypes`
--

INSERT INTO `acctypes` (`id`, `acc_desc`) VALUES
(1, 'Recoup Accounts'),
(2, 'Member Accounts');

-- --------------------------------------------------------

--
-- Table structure for table `memberaccmapping`
--

DROP TABLE IF EXISTS `memberaccmapping`;
CREATE TABLE `memberaccmapping` (
  `member_id` int(11) NOT NULL,
  `acc_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `onlinepayments`
--

DROP TABLE IF EXISTS `onlinepayments`;
CREATE TABLE `onlinepayments` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `onlinepayments`
--

INSERT INTO `onlinepayments` (`id`, `payment_id`, `payment_type`, `date_added`) VALUES
(1, 22, 1, '2021-03-05 15:47:57'),
(2, 23, 2, '2021-03-05 16:50:41');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `paypal_ref` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `app_ref` varchar(32) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_confirmed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(50) DEFAULT NULL,
  `payer_id` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `member_id`, `amount`, `paypal_ref`, `token`, `app_ref`, `date_added`, `date_confirmed`, `status`, `payer_id`) VALUES
(1, 1, '0.01', NULL, NULL, '3', '2021-03-01 18:34:45', '0000-00-00 00:00:00', NULL, NULL),
(2, 1, '0.01', '6ND15058P8114410W', 'EC-53F80896BB2634831', '3', '2021-03-01 18:37:54', '2021-03-01 18:38:21', 'Completed', '5QNZYCTPLPJJ4'),
(3, 1, '0.01', '4U196816JH5119001', 'EC-7X03080830200420D', '3', '2021-03-01 18:45:27', '2021-03-01 18:46:35', 'Completed', '5QNZYCTPLPJJ4'),
(4, 1, '0.01', NULL, 'EC-54K17255J1827451G', 'XBBA9EL3', '2021-03-03 07:01:12', '0000-00-00 00:00:00', NULL, NULL),
(5, 3, '0.01', NULL, 'EC-1CE72879WL160613S', 'MAZZIJLN', '2021-03-03 07:11:20', '0000-00-00 00:00:00', NULL, NULL),
(6, 3, '0.01', NULL, 'EC-78C62208GT834974P', 'TGWVLS5Q', '2021-03-03 07:13:38', '0000-00-00 00:00:00', NULL, NULL),
(7, 3, '0.01', NULL, 'EC-46P13581W46201408', '59PLU7TA', '2021-03-03 07:15:27', '0000-00-00 00:00:00', NULL, NULL),
(8, 3, '0.01', NULL, 'EC-28C68502KW7710804', 'H988A4DD', '2021-03-03 07:22:55', '0000-00-00 00:00:00', NULL, NULL),
(9, 3, '0.01', NULL, 'EC-9L546379DK8295027', '9VCMR32N', '2021-03-03 07:23:24', '0000-00-00 00:00:00', NULL, NULL),
(10, 3, '0.01', NULL, 'EC-9RL04715NP566300U', 'M3N27D9G', '2021-03-03 07:24:01', '0000-00-00 00:00:00', NULL, NULL),
(11, 3, '0.01', NULL, 'EC-1C0514979R021593V', 'CG7UXRJ1', '2021-03-03 07:24:23', '0000-00-00 00:00:00', NULL, NULL),
(12, 3, '0.01', NULL, 'EC-19S528026N7159053', 'OR4KWL7Z', '2021-03-03 07:26:31', '0000-00-00 00:00:00', NULL, NULL),
(13, 3, '0.01', NULL, 'EC-42D87955138664439', 'IP52OR2P', '2021-03-03 07:27:13', '0000-00-00 00:00:00', NULL, NULL),
(14, 3, '0.01', NULL, 'EC-04511574MY994902E', '2A0VU0GG', '2021-03-03 07:27:50', '0000-00-00 00:00:00', NULL, NULL),
(15, 3, '0.01', NULL, 'EC-72591870VB004100J', 'W23YLFR9', '2021-03-03 07:36:41', '0000-00-00 00:00:00', NULL, NULL),
(16, 3, '0.01', NULL, 'EC-1BE61492ND206932N', 'D6JUK3ID', '2021-03-03 07:37:28', '0000-00-00 00:00:00', NULL, NULL),
(17, 3, '0.01', NULL, 'EC-1K541489GJ913662M', 'PYMW94VF', '2021-03-03 07:37:55', '0000-00-00 00:00:00', NULL, NULL),
(18, 3, '0.01', NULL, 'EC-30746245UH067235E', 'G2EQEK7Q', '2021-03-03 07:38:08', '0000-00-00 00:00:00', NULL, NULL),
(19, 3, '0.01', '3LU62937L04254802', 'EC-35J65088B3604972K', 'BTBOZC88', '2021-03-03 07:44:10', '2021-03-03 07:45:36', 'Completed', '5QNZYCTPLPJJ4'),
(20, 3, '0.01', '4LW67389GA857013A', 'EC-3H856788GF368190S', 'LM9Y22YX', '2021-03-03 15:25:48', '2021-03-03 15:26:55', 'Completed', '5QNZYCTPLPJJ4'),
(21, 3, '0.01', '1ES50788239106814', 'EC-0TG21225S6308810U', '9UG1JMNM', '2021-03-03 15:35:08', '2021-03-03 15:35:30', 'Completed', '5QNZYCTPLPJJ4'),
(22, 3, '0.01', '5EN628158P691764U', 'EC-8352810260183425H', '47AIVZI1', '2021-03-03 15:36:28', '2021-03-03 15:36:53', 'Completed', '5QNZYCTPLPJJ4'),
(23, 3, '0.01', '4WK1543454890873X', 'EC-6EH884154B2797223', 'C6LP8W7T', '2021-03-03 16:02:40', '2021-03-03 16:03:28', 'Completed', '5QNZYCTPLPJJ4'),
(24, 3, '0.01', NULL, 'EC-88D883281D0800720', 'P74MDN7O', '2021-03-03 16:18:30', '0000-00-00 00:00:00', NULL, NULL),
(25, 3, '0.01', NULL, 'EC-9NP933398P230140L', 'U7VZCA9E', '2021-03-03 16:24:31', '0000-00-00 00:00:00', NULL, NULL),
(26, 3, '0.01', NULL, 'EC-5JM57514SE883621P', 'ACVDVB4L', '2021-03-03 16:25:53', '0000-00-00 00:00:00', NULL, NULL),
(27, 3, '0.01', NULL, 'EC-21N98379LC837891Y', 'OY5436DU', '2021-03-03 16:27:57', '0000-00-00 00:00:00', NULL, NULL),
(28, 3, '0.01', NULL, 'EC-0C49818239314242C', 'OBBSU03X', '2021-03-03 16:28:48', '0000-00-00 00:00:00', NULL, NULL),
(29, 3, '0.01', NULL, 'EC-8S263080TX674874Y', '95LVNZ5R', '2021-03-03 16:29:04', '0000-00-00 00:00:00', NULL, NULL),
(30, 3, '0.01', NULL, 'EC-3CU289626F085254C', 'H4Z3NXDU', '2021-03-03 16:30:57', '0000-00-00 00:00:00', NULL, NULL),
(31, 3, '0.01', NULL, 'EC-2WB5754972774691B', '4TRL73NU', '2021-03-03 16:32:30', '0000-00-00 00:00:00', NULL, NULL),
(32, 3, '0.01', NULL, 'EC-6TD53960XB783542P', '9TSKTL76', '2021-03-03 16:53:59', '0000-00-00 00:00:00', NULL, NULL),
(33, 3, '0.01', NULL, 'EC-4RP79499NG332891S', 'YSF2NQVV', '2021-03-03 16:56:20', '0000-00-00 00:00:00', NULL, NULL),
(34, 3, '0.01', NULL, 'EC-9X158931VD011442F', 'RN8TZ1PP', '2021-03-03 17:03:29', '0000-00-00 00:00:00', NULL, NULL),
(35, 3, '0.01', NULL, 'EC-6FL46207M9610813G', 'OHIS53OD', '2021-03-03 17:04:41', '0000-00-00 00:00:00', NULL, NULL),
(36, 3, '0.01', NULL, 'EC-2407546698013911P', 'JAG6715C', '2021-03-03 17:08:48', '0000-00-00 00:00:00', NULL, NULL),
(37, 3, '0.01', NULL, 'EC-3D44915313131944H', '04QYIAJ6', '2021-03-03 17:10:14', '0000-00-00 00:00:00', NULL, NULL),
(38, 3, '0.01', NULL, 'EC-4FL39905WJ830783E', 'ULJE9QUU', '2021-03-03 17:14:37', '0000-00-00 00:00:00', NULL, NULL),
(39, 3, '0.01', NULL, 'EC-2582126758757700L', 'PE7OPF0Z', '2021-03-03 17:16:23', '0000-00-00 00:00:00', NULL, NULL),
(40, 3, '0.01', NULL, 'EC-4MW92135Y9694835T', 'YOSL9NPB', '2021-03-03 17:20:02', '0000-00-00 00:00:00', NULL, NULL),
(41, 3, '0.01', NULL, 'EC-84J54320EK8949804', 'JVC8QWV8', '2021-03-03 17:21:22', '0000-00-00 00:00:00', NULL, NULL),
(42, 3, '0.01', NULL, 'EC-5WN592795M8635159', 'MMISVRI2', '2021-03-03 17:26:52', '0000-00-00 00:00:00', NULL, NULL),
(43, 3, '0.01', NULL, 'EC-40912155E7650174L', '3809K70Z', '2021-03-03 17:27:53', '0000-00-00 00:00:00', NULL, NULL),
(44, 3, '0.01', '6YG5700668117914L', 'EC-89B58628M5279802K', 'AD188NK0', '2021-03-03 17:28:20', '2021-03-03 17:29:24', 'Completed', '5QNZYCTPLPJJ4'),
(45, 3, '0.01', NULL, 'EC-1MK29414L5845074S', 'KU3JK46N', '2021-03-03 17:29:44', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `paymenttypes`
--

DROP TABLE IF EXISTS `paymenttypes`;
CREATE TABLE `paymenttypes` (
  `id` int(11) NOT NULL,
  `payment_desc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paymenttypes`
--

INSERT INTO `paymenttypes` (`id`, `payment_desc`) VALUES
(1, 'Paypal'),
(2, 'Card');

-- --------------------------------------------------------

--
-- Table structure for table `recoupcasesaccounts`
--

DROP TABLE IF EXISTS `recoupcasesaccounts`;
CREATE TABLE `recoupcasesaccounts` (
  `recoup_id` int(11) NOT NULL,
  `acc_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accstatement`
--
ALTER TABLE `accstatement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acctypes`
--
ALTER TABLE `acctypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `memberaccmapping`
--
ALTER TABLE `memberaccmapping`
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `acc_idx` (`acc_no`) USING BTREE;

--
-- Indexes for table `onlinepayments`
--
ALTER TABLE `onlinepayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymenttypes`
--
ALTER TABLE `paymenttypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recoupcasesaccounts`
--
ALTER TABLE `recoupcasesaccounts`
  ADD PRIMARY KEY (`recoup_id`),
  ADD UNIQUE KEY `ac_idx` (`acc_no`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `accstatement`
--
ALTER TABLE `accstatement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acctypes`
--
ALTER TABLE `acctypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `onlinepayments`
--
ALTER TABLE `onlinepayments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `paymenttypes`
--
ALTER TABLE `paymenttypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
