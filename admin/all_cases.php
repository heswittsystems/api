<?php


//var_dump($payments);



?>


<link href="css/dataTables.bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/fixedHeader.bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/responsive.bootstrap.min.css" rel='stylesheet' type='text/css' />

<link href="css/table.css" rel='stylesheet' type='text/css' />


<div class="blank-page">
  <h3>List of Cases</h3>
	<table id="example" class="table table-striped table-bordered ">
						  <thead>
							<tr>
							  <th>Date Created</th>
							  <th>Case Name</th>
							  <th>Amount</th>
							  <th>Recoups</th>
							  <th>Status</th>
							</tr>
						  </thead>
						  <tbody>
						  <?php
						  //$op = new Onlinepayments();
						  if($_GET['act'] == 'close')
						  {
							$wp =  Welfareprojects::find($_GET['wid']);
							$wp->status = 'Closed';
							$wp->save();


						  }
						  $cases = Welfareprojects::find('all',array('order'=>'id desc'));
						 // var_dump($payments);

						   foreach($cases as $case)
						   {
							 echo '<tr >';
							 echo '<th scope="row">'.date('m-d-Y', strtotime(ActiveRecord\Connection::datetime_to_string($case->date_added))).'</th>';
							//echo '<th scope="row">'.ActiveRecord\Connection::datetime_to_string($payment->date_added).'</th>';
							 echo '<td>'.$case->project_name.'</td>';
							 echo '<td>'.number_format($case->amount,2).'</td>';
							 echo '<td>'. total_recoups($case->id).'</td>';
							 if($case->status == 'Open')
							 {
							    echo '<td>'.$case->status.' | <a href=index.php?id=6&act=close&wid='.$case->id.'>Close</a></td>';
							 }
							 else
							 {
							   echo '<td>'.$case->status.'</td>';

							 }
							 echo '</tr>';
							   
						   }
						  ?>	
						 	
						  </tbody>
						</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#example').DataTable( {
        responsive: true,
	"order": [[ 0, "desc" ]]
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
</script>
<!--<script src="js/jquery-3.5.1.js"></script>-->
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script src="js/dataTables.fixedHeader.min.js"></script>
<script src="js/dataTables.responsive.min.js"></script>
<script src="js/responsive.bootstrap.min.js"></script>

