<?php


//var_dump($payments);
if(count($_POST) > 0)
{
//print_r($_POST);
   $admin_id = 2;
   new_chat( $admin_id,$_POST['subject'],$_POST['message'],$_POST['m_id']);
   new_notification_single($_POST['m_id'],$_POST['subject'],$_POST['message']);
}
?>


<link href="css/fixedHeader.bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/responsive.bootstrap.min.css" rel='stylesheet' type='text/css' />

<link href="css/table.css" rel='stylesheet' type='text/css' />


<div class="blank-page">
<div class="grid-form">
 		<div class="grid-form1">
 		<h4 id="forms-example" class="">Create New Message to Member</h4>
 		<form onsubmit="return confirm('Do you really want to send the message?')" action="" method="post"  name="RequestCreateForm" id="RequestCreateForm">
  <div class="form-group">
    <label for="mem">Select Member</label>
    <select name="m_id" id="mem" class="form-control">
           <option value=-1 >Select member to send message to </option>
            <?php
              $all = Gdnusers::find('all',array('order'=>'name asc'));
              foreach($all as $a)
              {
                echo '<option value='.$a->id.'>'.$a->name.'</option>';
              }
            ?>
        </select>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Subject of message</label>
    <input type="text" class="form-control" id="exampleInputEmail1" required name="subject" placeholder="A short heading for the message">
  </div>
  <div class="form-group">
    <label for="amount">Message</label>
    <input type="text" class="form-control" required id="amount" name="message" placeholder="Enter the full message">
  </div>
  <div class="form-group">
    <input type="submit"   name="add" value="Send Message" class="btn btn-primary" style="background-color: #4CAF50;" >
  </div>
</form>
</div>
</div>
<h4>Current Messages from members</h4>
	<table id="example" class="table table-striped table-bordered ">
						  <thead>
							<tr>
							  <th>Date</th>
							  <th>Subject</th>
							  <th>Created By</th>
							  <th>Recipient</th>
							</tr>
						  </thead>
						  <tbody>
						  <?php
						  $chats = Messagesubjects::find('all',array('order'=>'id desc'));
						   //var_dump($payments);

						   foreach($chats as $chat)
						   {
							   
							 echo '<tr >';
							 echo '<th scope="row">'.date('m-d-Y', strtotime(ActiveRecord\Connection::datetime_to_string($chat->created_date))).'</th>';
							 //echo '<td><a href=index.php?id=13&cid='.$chat->id.">".$chat->subject.' <span class="badge badge-pill badge-success" style="background-color: #4CAF50;">3</span></a></td>';
							 echo '<td><a href=index.php?id=13&cid='.$chat->id.">".$chat->subject.'</a></td>';
							 echo '<td>'. Gdnusers::find($chat->created_by)->name.'</td>';
							 echo '<td>'.Gdnusers::find($chat->recipient_id)->name.'</td>';
							 echo '</tr>';
							   
						   }
						  ?>	
						 	
						  </tbody>
						</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#example').DataTable( {
        responsive: true,
	"order": []
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
</script>
<!--<script src="js/jquery-3.5.1.js"></script>-->
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script src="js/dataTables.fixedHeader.min.js"></script>
<script src="js/dataTables.responsive.min.js"></script>
<script src="js/responsive.bootstrap.min.js"></script>

