<?php


//var_dump($payments);



?>


<link href="css/dataTables.bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/fixedHeader.bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/responsive.bootstrap.min.css" rel='stylesheet' type='text/css' />

<link href="css/table.css" rel='stylesheet' type='text/css' />


<div class="blank-page">
	<table id="example" class="table table-striped table-bordered ">
						  <thead>
							<tr>
							  <th>Date</th>
							  <th>Member</th>
							  <th>Method</th>
							  <th>Amount</th>
							</tr>
						  </thead>
						  <tbody>
						  <?php
						  //$op = new Onlinepayments();
						  $payments = Onlinepayments::find('all',array('order'=>'id desc'));
						 // var_dump($payments);

						   foreach($payments as $payment)
						   {
							 $data = get_payment_data($payment->payment_id,$payment->payment_type);  
							 echo '<tr >';
							 echo '<th scope="row">'.date('m-d-Y', strtotime(ActiveRecord\Connection::datetime_to_string($payment->date_added))).'</th>';
							//echo '<th scope="row">'.ActiveRecord\Connection::datetime_to_string($payment->date_added).'</th>';
							 echo '<td><a href=index.php?id=11&m_id='.trim($data->member_id).">".get_member_name($payment->payment_id,$payment->payment_type).'</a></td>';
							 echo '<td>'.get_payment_method($payment->payment_type).'</td>';
							 echo '<td>'.number_format($data->amount,2).'</td>';
							 echo '</tr>';
							   
						   }
						  ?>	
						 	
						  </tbody>
						</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#example').DataTable( {
        responsive: true,
	"order": [[ 0, "desc" ]]
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
</script>
<!--<script src="js/jquery-3.5.1.js"></script>-->
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script src="js/dataTables.fixedHeader.min.js"></script>
<script src="js/dataTables.responsive.min.js"></script>
<script src="js/responsive.bootstrap.min.js"></script>

