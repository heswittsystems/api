<style type="text/css">

.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
  display: none;
   margin: 0 auto;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}




</style>
<div class="grid-form">
 		<div class="grid-form1">
 		<h3 id="forms-example" class="">Create New Event</h3>
 		<form onsubmit="return confirm('Do you really want to add the event?')" action="" method="post"  name="RequestCreateForm" id="RequestCreateForm">
  <div class="form-group">
    <label for="exampleInputEmail1">Event title</label>
    <input type="text" class="form-control" id="exampleInputEmail1" required name="subject" placeholder="Enter subject of the event">
  </div>
  <div class="form-group">
    <label for="ann">Event Details</label>
    <input type="text" class="form-control" required id="ann" name="msg" placeholder="Enter the event details">
  </div>
  <div class="form-group">
    <label for="datetime">Event Date and Time</label>
    <input type="text" class="form-control" required id="datetime" name="datetime" placeholder="Select the event date and time">
  </div>
  <div class="form-group">
    <input type="submit" onclick="document.getElementById('loader').style.display = 'block';"  name="add" value="Add Event" class="btn btn-primary" style="background-color: #4CAF50;" >
  </div>
</form>
<div align="center" id="loader" class="loader"></div>
<?php
if($_POST['add'])
{
     ?>
<script type="text/javascript">document.getElementById('loader').style.display = 'block';</script>
<?php
     //print_r($_POST);
     //ob_flush();
     try
     {
     $event_date = strtotime($_POST['datetime']);
     if($event_date === false)
     {
        throw new \Exception('Date foemating error');

     }
     $an = new Events(array('event_title'=>$_POST['subject'],'event_details'=>$_POST['msg'],'event_date'=>$event_date));
     $an->save();
     //exit;
     //var_dump($wp);
     $members = Memberaccmappings::find('all');
     $i = 0;
     $title = $_POST['subject'];//"New Case: ".$_POST['case'];
     $body = $_POST['msg'];
     $to = array();
     foreach($members as $member)
     {
         $notice = new Notifications(array('title'=>$title,'details'=>$body,'member_id'=>$member->member_id));

	 $notice->save();
         try
	 {
	   $token =  Pushtokens::find($member->member_id)->push_token;
	   $to[] = '"'.$token.'"';
	//var_dump($member);
	 }
	catch(Exception $e)
	{


	}
	   

     }

     $all = array_chunk($to,90);
     foreach($all as $a)
     {

        $tokens = implode(",",$a);
	send_push_notification($tokens,$title,$body);

     }
     ?>
<script type="text/javascript">document.getElementById('loader').style.display = 'none';</script>
    <div class="alert alert-success" role="alert">
      New Event added and notifications sent to members
      </div>
<?php
    }
    catch(Exception $e)
    {
      var_dump($e);
         ?>
	 <div class="alert alert-danger" role="alert">
	   An error has occurred. 
	   </div>
	   <?php

    }
}
?>
</div>
</div>
<script src="js/pickaday.js"></script>
<script>
    var picker = new Pikaday({ field: document.getElementById('datetime') });
    </script>
