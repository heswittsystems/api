<?php


//var_dump($payments);
if($_GET['app'])
{
  $payment =   Offlinepayments::find($_GET['pid']);
  $payment->tx_status = $_GET['app'];
  $payment->save();
  if($_GET['app'] == 2)//accepted add to account and check recoups
  {
     $desc = Offlinepaymentmethods::find($payment->method_id)->method_desc." Deposit: ".$payment->tx_ref;
     $ref = gen_tx_ref();
     new_deposit($payment->member_id,$payment->amount,$ref,$desc);


  }

}




?>


<link href="css/dataTables.bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/fixedHeader.bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/responsive.bootstrap.min.css" rel='stylesheet' type='text/css' />

<link href="css/table.css" rel='stylesheet' type='text/css' />


<div class="blank-page">
	<table id="example" class="table table-striped table-bordered ">
						  <thead>
							<tr>
							  <th>Date Paid</th>
							  <th>Member</th>
							  <th>Method</th>
							  <th>Amount</th>
							  <th>Ref.</th>
							  <th>Status</th>
							  <th>Action</th>
							</tr>
						  </thead>
						  <tbody>
						  <?php
						  //$op = new Onlinepayments();
						  $status[1] = "Pending";
						  $status[2] = "Received";
						  $status[3] = "Rejected";
						  $payments = Offlinepayments::find('all',array('order'=>'id desc'));
						 // var_dump($payments);

						   foreach($payments as $payment)
						   {
							 echo '<tr >';
							 echo '<th scope="row">'.$payment->date_paid.'</th>';
							//echo '<th scope="row">'.ActiveRecord\Connection::datetime_to_string($payment->date_added).'</th>';
							 echo '<td><a href=index.php?id=11&m_id='.trim($payment->member_id).">".get_member_name_id($payment->member_id).'</a></td>';
							 echo '<td>'.Offlinepaymentmethods::find($payment->method_id)->method_desc.'</td>';
							 echo '<td>'.number_format($payment->amount,2).'</td>';
							 echo '<td>'.$payment->tx_ref.'</td>';
							 echo '<td>'.$status[$payment->tx_status].'</td>';
							 if($payment->tx_status == 1)
							 {
								 echo '<td>';
								 echo '<a href=index.php?id=2&app=2&pid='.$payment->id.'><i class="fa fa-check-circle-o fa-2x" style="color:green"></i></a>';

								 echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
								 echo '<a href=index.php?id=2&app=3&pid='.$payment->id.'><i class="fa fa-times-circle-o fa-2x" style="color:red"></i></a>';
								 echo '</td>';
							 }
							 else
							 {
						            echo '<td>'.$status[$payment->tx_status].'</td>';

							 }
							 echo '</tr>';
							   
						   }
						  ?>	
						 	
						  </tbody>
						</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#example').DataTable( {
        responsive: true,
	"order": []
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
</script>
<!--<script src="js/jquery-3.5.1.js"></script>-->
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script src="js/dataTables.fixedHeader.min.js"></script>
<script src="js/dataTables.responsive.min.js"></script>
<script src="js/responsive.bootstrap.min.js"></script>

