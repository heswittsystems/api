<?php

require_once __DIR__.'/vendor/autoload.php';
    
    $channelName = 'news';
    $recipient= 'ExponentPushToken[0bbxb6AisJo-6sqjAeri3w]';
    
    // You can quickly bootup an expo instance
    $expo = \ExponentPhpSDK\Expo::normalSetup();
    
    // Subscribe the recipient to the server
    $expo->subscribe($channelName, $recipient);
    
    // Build the notification data
    $notification = ['body' => 'Hello World!'];
    
    // Notify an interest with a notification
    $rep = $expo->notify([$channelName], $notification);
    var_dump($rep);

?>
