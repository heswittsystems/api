<?php
require_once dirname(__FILE__) . '/../ncko-db/ActiveRecord.php';


// initialize ActiveRecord
ActiveRecord\Config::initialize(function($cfg)
{
    $cfg->set_model_directory(dirname(__FILE__) . '/../ncko-db/models');
    $cfg->set_connections(array('development' => 'mysql://root:dustbin@localhost/ncko'));

	// you can change the default connection with the below
    //$cfg->set_default_connection('production');
});

function new_deposit($member_id,$amount,$ref,$desc)
{
   update_account( get_account_no($member_id),$amount,$ref,$desc,'CREDIT');
   recoup_on_new_deposit($member_id);

}

function get_account_no($member_id)
{
   try
   {
     error_log("select * from memberaccmappings where member_id='$member_id'");
      $acc = Memberaccmappings::find_by_sql("select * from memberaccmappings where member_id='$member_id'");
    //  var_dump($acc);
     if(count($acc) > 0)
     {
         $acc_num =  $acc[0]->acc_no;
     }
   }
   catch(Exception $e) {
      error_log("No account");
      $e->getMessage();
   }
   
   if($acc_num)
   {
      return $acc_num;
   }

   return gen_account($member_id);


}

function gen_account($member_id)
{

    $new_acc = new Accounts(array('acc_type'=>2));
    $acc = $new_acc->save();
    error_log("********************$acc"); 
    $new_acc = Accounts::last()->id;
//    echo "wtf";
   // var_dump( $new_acc);
    $m = new Memberaccmappings(array('member_id'=>$member_id,'acc_no'=> $new_acc));
    $m->save();

    return $new_acc;

}

function update_account($account_no,$amount,$ref,$desc,$side)
{

  
  error_log("select * from accstatements where acc_no='$account_no' order by id desc limit 1 $account_no,$amount,$ref,$desc,$side");
  $rec = Accstatements::find_by_sql("select * from accstatements where acc_no='$account_no' order by id desc limit 1");
  $last_rec = $rec[0];
  $old_balance = $last_rec->balance;
  $side = strtoupper($side);
  if($side == 'DEBIT')
  {
    $new_balance = $old_balance - $amount;
    if($new_balance < 0)
    {
      return -1;

    }
    $new_rec = new Accstatements(array('acc_no'=>$account_no,'ref'=>$ref,'tx_desc'=>$desc,'debit'=>$amount,'credit'=>0,'balance'=>$new_balance));
  }
  if($side == 'CREDIT')
  {
     $new_balance = $old_balance + $amount;
     $new_rec = new Accstatements(array('acc_no'=>$account_no,'ref'=>$ref,'tx_desc'=>$desc,'debit'=>0,'credit'=>$amount,'balance'=>$new_balance));
  }
  $new_rec->save();


} 

function new_online_payment($payment_id,$payment_type)
{
   $pp = new Onlinepayments(array('payment_id'=>$payment_id,'payment_type'=>$payment_type));
   $pp->save();

}
function gen_tx_ref()
{
   $key = '';
   $length = 8;
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return strtoupper($key);

}
function new_card_tx($member_id,$amount,$tx_status,$payment_ref)
{
  $ref = gen_tx_ref();
  $cp = new Cardpayments(array('member_id'=>$member_id,'app_ref'=>$ref,'payment_ref'=>$payment_ref,'amount'=>$amount,'status'=>$tx_status));
  $cp->save();
  $id = Cardpayments::find('last')->id;
  
  new_online_payment($id,2);
  new_deposit($member_id,$amount,$ref,"Card Transaction: ".strtoupper($payment_ref));


}

function get_balance($member_id)
{
   $account = get_account_no($member_id);
   error_log("fecthed acount $account");
   //echo "select * from accstatements where acc_no='$account' order by id desc limit 1";
   $rec = Accstatements::find_by_sql("select * from accstatements where acc_no='$account' order by id desc limit 1");
   //var_dump($rec);
   if(count($rec) < 1)
   {
           //new acc so no records
      $new_rec = new Accstatements(array('acc_no'=>$account,'ref'=>gen_tx_ref(),'tx_desc'=>'Acccount Opening','debit'=>0,'credit'=>0,'balance'=>0));
      $new_rec->save();
      $bal = 0;

   }
   else
   {
      $bal = $rec[0]->balance;
   }

   //return number_format($bal,2);
   return $bal;
}

function date_string($dt)
{
  //$d = date('m-d-Y', strtotime(ActiveRecord\Connection::datetime_to_string($dt)));
  $d = date('F j, Y', strtotime(ActiveRecord\Connection::datetime_to_string($dt)));
  return $d;
}

function case_recoup($member_id,$case_id)
{
//when a new case is created, recoup all by calling this function with all member ids
  $case = Welfareprojects::find($case_id); 
  $amount = $case->amount;
  $balance = get_balance($member_id);
  if($balance < $amount )
  {
     //send notification by firebase
     $title = "Insufficient Funds";
     $body = "Recoup for ".$case->project_name." for USD $amount failed. Please add funds to your account";
     new_notification_single($member_id,$title,$body);
     return;
  }
  $name = Gdnusers::find($member_id)->name;
  $recoup_acc = Recoupcasesaccounts::find($case_id)->acc_no;
  $member_acc = get_account_no($member_id);
  $proj = $case->project_name;
  $desc = "Recoup: $proj";
  $ref = gen_tx_ref();
  update_account($recoup_acc,$amount,$ref,"Recoup: $name",'Credit');//recoup case
  update_account($member_acc,$amount,$ref,$desc,'Debit');//member id
  $recoup = new Caserecoups(array('member_id'=>$member_id,'case_id'=>$case_id));
  $recoup->save();
     $title = "Recoup Successfull";
     $body = "Recoup for ".$case->project_name." for USD $amount successfull";
     new_notification_single($member_id,$title,$body);


}
function recoup_on_new_deposit($member_id)
{
 //find all open cases where member has not recouped 
  $cases = Welfareprojects::find_by_sql("select * from welfareprojects where id not in (select case_id from caserecoups where member_id='$member_id' order by id asc)");
  foreach($cases as $case)
  {
     case_recoup($member_id,$case->id);


  }
    

}

function new_case_recoup($case_id)
{
//select all members and recoup from their accounts // skip if insuffience balance todo: //generate notification


}

function gen_recoup_acc($case_id)
{

    $new_acc = new Accounts(array('acc_type'=>1));
    $acc = $new_acc->save();
    error_log("********************$acc"); 
    $new_acc = Accounts::last()->id;
//    echo "wtf";
  //  var_dump( $new_acc);
    $m = new Recoupcasesaccounts(array('recoup_id'=>$case_id,'acc_no'=> $new_acc));
    $m->save();
    $desc = "Account Opening";
    $ref = gen_tx_ref();
    update_account($new_acc,0,$ref,$desc,'Credit');//recoup case

    return $acc;

}

//gen_recoup_acc(1);
//case_recoup(2,1);
function new_notification_single($member_id,$title,$body)
{
  $notice = new Notifications(array('title'=>$title,'details'=>$body,'member_id'=>$member_id));
  $notice->save();
  try
  {
    $token = Pushtokens::find($member_id)->push_token;
    send_push_notification('"'.$token.'"',$title,$body);
  }
  catch(Exception $e)
  {
    error_log("token not found for $member_id body is $title,$body");
  //  var_dump($e);

  }
}

function send_push_notification($to,$title,$body)
{
//$to must be double quotes encased
 $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://exp.host/--/api/v2/push/send',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'[
  
  {
    "to": [
      '.$to.'
    ],
    "body": "'.$body.'",
    "title": "'.$title.'"
  }
]',
  CURLOPT_HTTPHEADER => array(
    'Accept: application/json',
    'Accept-Encoding: gzip,deflate',
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);

//var_dump( $response);



}

function total_recoups($case_id)
{
  
  $recs = Caserecoups::find_by_sql("select count(*) as total from caserecoups where case_id='$case_id'");
  return $recs[0]->total;



}

function new_chat($user_id,$subject,$msg,$recipient_id)
{
    $chat = new Messagesubjects(array('subject'=>$subject,'recipient_id'=>$recipient_id,'created_by'=>$user_id));
    $chat->save();

    $message = new  Messages(array('subject_id'=>$chat->id,'seq_no'=>1,'created_by'=>$user_id,'message'=>$msg));
    $message->save(); 

}

function update_chat($subject_id,$created_by,$message)
{
   $seq = Messages::find_by_sql("select max(seq_no) as num from messages where subject_id='$subject_id'");
   $new_seq_no = $seq[0]->num + 1;
   
   $message = new  Messages(array('subject_id'=>$subject_id,'seq_no'=>$new_seq_no,'created_by'=>$created_by,'message'=>$message));
   $message->save(); 
   

}


?>
